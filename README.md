Pluggable Annotation Processing API Sample
===========================================================

Pluggable Annotation Processing API Core を拡張したサンプルAPIです。

## LICENSE
Licensed under the [Apache License, Version 2.0][Apache]
[Apache]: http://www.apache.org/licenses/LICENSE-2.0
