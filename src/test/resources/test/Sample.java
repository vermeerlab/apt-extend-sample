/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package test;

import org.vermeerlab.annotation.processor.sample.TargetClass;
import org.vermeerlab.annotation.processor.sample.TargetField;

/**
 *
 * @author Yamashita,Takahiro
 */
@TargetClass
public class Sample {

    @TargetField
    private final String _name;

    @TargetField
    private final String _desc;

    public Sample(String name, String desc) {
        _name = name;
        _desc = desc;
    }

    public String getName() {
        return _name;
    }

    public String getDesc() {
        return _desc;
    }
}
