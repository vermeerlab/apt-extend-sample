/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.annotation.processor.sample;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import org.vermeerlab.apt.AnnotationProcessorException;
import org.vermeerlab.apt.command.ProcessorCommandInterface;
import org.vermeerlab.base.DebugUtil;

/**
 *
 * @author Yamashita,Takahiro
 */
public class SampleCommand implements ProcessorCommandInterface {

    @Override
    public Class<? extends Annotation> getTargetAnnotation() {
        return TargetClass.class;
    }

    @Override
    public void execute(ProcessingEnvironment processingEnvironment, Element element, Boolean isDebug) {
        ArgumentInfo argInfo = getArgumentInfo(element);

        MethodSpec.Builder builder = MethodSpec.methodBuilder("create")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(TypeName.get(element.asType()))
                .addStatement("return new $T(" + argInfo.join() + ")",
                              TypeName.get(element.asType()));
        for (String arg : argInfo.getArgNames()) {
            builder.addParameter(String.class, arg);
        }
        MethodSpec create = builder.build();

        String className = element.getSimpleName() + "Factory";
        TypeSpec factory = TypeSpec.classBuilder(className)
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addMethod(create)
                .build();

        JavaFile javaFile = JavaFile.builder(
                getPackageName(element), factory).
                build();

        //生成したソースを確認したいので、コンソールに直接出力してみる
        DebugUtil debugUtil = new DebugUtil(isDebug);
        debugUtil.print(javaFile.toString());

        Filer _filer = processingEnvironment.getFiler();
        try {
            javaFile.writeTo(_filer);
        } catch (IOException ex) {
            throw new AnnotationProcessorException(ex, "Sample file could not write");
        }
    }

    private String getPackageName(Element element) {
        List<String> packNames = new ArrayList<>();
        Element packageElem = element.getEnclosingElement();
        while (packageElem != null) {
            String packName = packageElem.
                    getSimpleName().toString();
            packNames.add(packName);
            packageElem = packageElem.getEnclosingElement();
        }

        StringBuilder sb = new StringBuilder();
        for (int i = packNames.size() - 1; i >= 0; i--) {
            if (sb.length() > 0) {
                sb.append(".");
            }
            sb.append(packNames.get(i));
        }
        return sb.toString();
    }

    private ArgumentInfo getArgumentInfo(Element element) {
        ArgumentInfo argInfo = new ArgumentInfo();
        for (Element e : element.getEnclosedElements()) {
            if (e.getAnnotation(TargetField.class) != null) {
                argInfo.add(e.getSimpleName().toString());
            }
        }
        return argInfo;
    }

    private static class ArgumentInfo {

        private final List<String> _argNames = new ArrayList<>();

        public void add(String argName) {
            _argNames.add(argName);
        }

        public String[] getArgNames() {
            return _argNames.toArray(new String[0]);
        }

        public String join() {
            StringBuilder sb = new StringBuilder();
            for (String argName : _argNames) {
                if (sb.length() > 0) {
                    sb.append(",");
                }
                sb.append(argName);
            }
            return sb.toString();
        }
    }
}
